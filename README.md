# Test task application: search in files with texts by special expressions

## Description

The application searches through the files by the given expression.

Supports only english texts.

In response the files that match the search query are displayed, indicating the results for all functions
(even if no results were found)

The application uses the following terminology:

* query - expression sent to server for search
* expression - various combinations of functions and operations
* function - terminal rule for text
* operation - rule to modify behavior of functions

## Project Structure

The project root consists of 3 folders:

* protobufs - directory with protobuf files used for communication between services
* sourceTexts - default source texts directory, contains example files
* services - contains all services

The application consists of 3 services at the services' folder, communication between which is done via grpc:

* searcher - main search service with web ui and rest api
* lemmatizer - lemmatization service
* synonymizer - synonymization service

## Running the application

### Local

To run locally, you will need to start all services by yourself:

1. Lemmatizer - see [readme](/services/lemmatizer/README.md)

2. Synonymizer - see [readme](/services/synonymizer/README.md)

3. Searcher - see [readme](/services/searcher/README.md)

### Docker

Ensure that you have installed docker v3+ and docker-compose v3+

Run the application:

```
docker-compose build
docker-compose up
```

## Application Usage

After starting the service by default it becomes available on [localhost:50100](http://localhost:50100/)
See description of use in [search service readme](/services/searcher/README.md)