# Lemmatization service

## Description

The service searches for all possible lemmas of the transmitted word regardless of the form of the word.

Communication with the service is carried out via rpc with messages by [protobuf](../../protobufs/lemmatizer.proto).

If none lemmas are found, the word itself will be returned.

## Libraries used

The service is based on grpc and nltk. See also [requirements.txt](requirements.txt)

## Running the service

### Local

To run the service:

* Go to the appropriate directory:
   ```
   cd services/lemmatizer
   ```
* Update pip to prevent possible errors:
   ```
   python -m pip install --upgrade pip
   ```
* Install required dependencies:
   ```
   python -m pip install -r requirements.txt
   ```
* Generate Python code from the protobufs:
   ```
   python -m grpc_tools.protoc -I ../../protobufs --python_out=. --grpc_python_out=. ../../protobufs/lemmatizer.proto
   ```
* Run the service:
   ```
   python run lemmatizer.py
   ```

Now the service is available on 50052 port

### Docker

To run the service:

```
docker-compose build lemmatizer
docker-compose up lemmatizer
```

Now the service is available on 50052 port
