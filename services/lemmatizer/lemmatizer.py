from concurrent import futures

import grpc
import nltk
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer

import lemmatizer_pb2_grpc
from lemmatizer_pb2 import LemmaResponse

nltk.download("wordnet", "nltk_data")
nltk.data.path.append('./nltk_data/')

poses = [wordnet.ADJ, wordnet.NOUN, wordnet.VERB, wordnet.ADV]
lemmatizer = WordNetLemmatizer()


class Lemmatizer(lemmatizer_pb2_grpc.lemmatizerServicer):
    def getLemmas(self, request, context):
        word = request.word.lower()
        lemmas = set()
        for pos in poses:
            related_lemma_name = lemmatizer.lemmatize(word, pos)
            if related_lemma_name not in lemmas:
                lemmas.add(related_lemma_name)

        if not len(lemmas):
            lemmas.add(word)

        return LemmaResponse(lemmas=lemmas)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    lemmatizer_pb2_grpc.add_lemmatizerServicer_to_server(
        Lemmatizer(), server
    )
    server.add_insecure_port("[::]:50052")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
