# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: lemmatizer.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='lemmatizer.proto',
  package='com.randomm.proto',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x10lemmatizer.proto\x12\x11\x63om.randomm.proto\"\x1c\n\x0cLemmaRequest\x12\x0c\n\x04word\x18\x01 \x01(\t\"\x1f\n\rLemmaResponse\x12\x0e\n\x06lemmas\x18\x01 \x03(\t2\\\n\nlemmatizer\x12N\n\tgetLemmas\x12\x1f.com.randomm.proto.LemmaRequest\x1a .com.randomm.proto.LemmaResponseb\x06proto3'
)




_LEMMAREQUEST = _descriptor.Descriptor(
  name='LemmaRequest',
  full_name='com.randomm.proto.LemmaRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='word', full_name='com.randomm.proto.LemmaRequest.word', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=39,
  serialized_end=67,
)


_LEMMARESPONSE = _descriptor.Descriptor(
  name='LemmaResponse',
  full_name='com.randomm.proto.LemmaResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='lemmas', full_name='com.randomm.proto.LemmaResponse.lemmas', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=69,
  serialized_end=100,
)

DESCRIPTOR.message_types_by_name['LemmaRequest'] = _LEMMAREQUEST
DESCRIPTOR.message_types_by_name['LemmaResponse'] = _LEMMARESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

LemmaRequest = _reflection.GeneratedProtocolMessageType('LemmaRequest', (_message.Message,), {
  'DESCRIPTOR' : _LEMMAREQUEST,
  '__module__' : 'lemmatizer_pb2'
  # @@protoc_insertion_point(class_scope:com.randomm.proto.LemmaRequest)
  })
_sym_db.RegisterMessage(LemmaRequest)

LemmaResponse = _reflection.GeneratedProtocolMessageType('LemmaResponse', (_message.Message,), {
  'DESCRIPTOR' : _LEMMARESPONSE,
  '__module__' : 'lemmatizer_pb2'
  # @@protoc_insertion_point(class_scope:com.randomm.proto.LemmaResponse)
  })
_sym_db.RegisterMessage(LemmaResponse)



_LEMMATIZER = _descriptor.ServiceDescriptor(
  name='lemmatizer',
  full_name='com.randomm.proto.lemmatizer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=102,
  serialized_end=194,
  methods=[
  _descriptor.MethodDescriptor(
    name='getLemmas',
    full_name='com.randomm.proto.lemmatizer.getLemmas',
    index=0,
    containing_service=None,
    input_type=_LEMMAREQUEST,
    output_type=_LEMMARESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_LEMMATIZER)

DESCRIPTOR.services_by_name['lemmatizer'] = _LEMMATIZER

# @@protoc_insertion_point(module_scope)
