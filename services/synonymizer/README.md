# Synonymization service

## Description

The service searches for lemmas of all synonyms of the given word

Communication with the service is carried out via rpc with messages by [protobuf](../../protobufs/synonymizer.proto).

If no lemma of synonyms is found, the lemma of the given word will be returned (in the absence of a lemma, the word
itself).

## Libraries used

The service is based on grpc and nltk. See also [requirements.txt](requirements.txt)

## Запуск

### Local

To run the service:

* Go to the appropriate directory:
   ```
   cd services/synonymizer
   ```
* Update pip to prevent possible errors:
   ```
   python -m pip install --upgrade pip
   ```
* Install required dependencies:
   ```
   python -m pip install -r requirements.txt
   ```
* Generate Python code from the protobufs:
   ```
   python -m grpc_tools.protoc -I ../../protobufs --python_out=. --grpc_python_out=. ../../protobufs/synonymizer.proto
   ```
* Run the service:
   ```
   python run synonymizer.py
   ```

Now the service is available on 50051 port

### Docker

To run the service:

```
docker-compose build synonymizer
docker-compose up synonymizer
```

Now the service is available on 50051 port