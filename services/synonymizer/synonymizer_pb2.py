# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: synonymizer.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='synonymizer.proto',
  package='com.randomm.proto',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x11synonymizer.proto\x12\x11\x63om.randomm.proto\"\x1e\n\x0eSynonymRequest\x12\x0c\n\x04word\x18\x01 \x01(\t\"#\n\x0fSynonymResponse\x12\x10\n\x08synonyms\x18\x01 \x03(\t2i\n\x0bsynonymizer\x12Z\n\x11getSynonymsLemmas\x12!.com.randomm.proto.SynonymRequest\x1a\".com.randomm.proto.SynonymResponseb\x06proto3'
)




_SYNONYMREQUEST = _descriptor.Descriptor(
  name='SynonymRequest',
  full_name='com.randomm.proto.SynonymRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='word', full_name='com.randomm.proto.SynonymRequest.word', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=40,
  serialized_end=70,
)


_SYNONYMRESPONSE = _descriptor.Descriptor(
  name='SynonymResponse',
  full_name='com.randomm.proto.SynonymResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='synonyms', full_name='com.randomm.proto.SynonymResponse.synonyms', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=107,
)

DESCRIPTOR.message_types_by_name['SynonymRequest'] = _SYNONYMREQUEST
DESCRIPTOR.message_types_by_name['SynonymResponse'] = _SYNONYMRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SynonymRequest = _reflection.GeneratedProtocolMessageType('SynonymRequest', (_message.Message,), {
  'DESCRIPTOR' : _SYNONYMREQUEST,
  '__module__' : 'synonymizer_pb2'
  # @@protoc_insertion_point(class_scope:com.randomm.proto.SynonymRequest)
  })
_sym_db.RegisterMessage(SynonymRequest)

SynonymResponse = _reflection.GeneratedProtocolMessageType('SynonymResponse', (_message.Message,), {
  'DESCRIPTOR' : _SYNONYMRESPONSE,
  '__module__' : 'synonymizer_pb2'
  # @@protoc_insertion_point(class_scope:com.randomm.proto.SynonymResponse)
  })
_sym_db.RegisterMessage(SynonymResponse)



_SYNONYMIZER = _descriptor.ServiceDescriptor(
  name='synonymizer',
  full_name='com.randomm.proto.synonymizer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=109,
  serialized_end=214,
  methods=[
  _descriptor.MethodDescriptor(
    name='getSynonymsLemmas',
    full_name='com.randomm.proto.synonymizer.getSynonymsLemmas',
    index=0,
    containing_service=None,
    input_type=_SYNONYMREQUEST,
    output_type=_SYNONYMRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_SYNONYMIZER)

DESCRIPTOR.services_by_name['synonymizer'] = _SYNONYMIZER

# @@protoc_insertion_point(module_scope)
