from concurrent import futures

import grpc
import nltk
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from itertools import chain

import synonymizer_pb2_grpc
from synonymizer_pb2 import SynonymResponse

nltk.download("wordnet", "nltk_data")
nltk.data.path.append('./nltk_data/')

all_synsets = {}
lemmas_in_wordnet = set(chain(*[x.lemma_names() for x in wordnet.all_synsets()]))

for lemma in lemmas_in_wordnet:
    all_synsets[lemma] = wordnet.synsets(lemma)

lemmatizer = WordNetLemmatizer()


class Synonymizer(synonymizer_pb2_grpc.synonymizerServicer):

    def getSynonymsLemmas(self, request, context):
        word_lemma = lemmatizer.lemmatize(request.word.lower())
        lemmas = set()
        if word_lemma in all_synsets:
            for synset in all_synsets[word_lemma]:
                lemmas.update(synset.lemma_names())
        else:
            lemmas.add(word_lemma)

        return SynonymResponse(synonyms=lemmas)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    synonymizer_pb2_grpc.add_synonymizerServicer_to_server(
        Synonymizer(), server
    )
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
