package com.randomm.searcher.controller;

import com.randomm.searcher.BaseIT;
import com.randomm.searcher.model.FileMatch;
import com.randomm.searcher.model.Interval;
import com.randomm.searcher.model.SearchResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class SearchControllerIT extends BaseIT {

    @DisplayName("/search api should find results as expected")
    @ParameterizedTest(name = "{index}: '{0}' query: ''{1}''")
    @MethodSource("searchPositiveDataProvider")
        //First arg passed only for test naming purposes
    void searchPositive(String testName, String query, SearchResponse expectedResponse) throws Exception {
        String response = mockMvc.perform(get("/search")
                        .queryParam("query", query))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        SearchResponse actual = mapper.readValue(response, SearchResponse.class);

        assertThat(actual.getFiles()).hasSameElementsAs(expectedResponse.getFiles());
    }

    @DisplayName("/search api should return bad request for bad query")
    @Test
    void searchExceptionally() throws Exception {
        mockMvc.perform(get("/search")
                        .queryParam("query", "bobo"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage", startsWith("Wrong query: ")));
    }

    private static Stream<Arguments> searchPositiveDataProvider() {
        return Stream.of(
                arguments("Base lemm query", "lemm(he)",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("1.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("lemm(he)", intervalsOf(new Interval(0, 2)))
                                        .build())
                                .build()),
                arguments("Parenthesis lemm query", "((lemm(he)))",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("1.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("lemm(he)", intervalsOf(new Interval(0, 2)))
                                        .build())
                                .build()),
                arguments("Not lemm query", "not(lemm(he))",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("2.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("lemm(he)", intervalsOf())
                                        .build())
                                .build()),
                arguments("Base syn query", "syn(tough)",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("2.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("syn(tough)", intervalsOf(new Interval(13, 16)))
                                        .build())
                                .build()),
                arguments("Parenthesis syn query", "((syn(tough)))",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("2.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("syn(tough)", intervalsOf(new Interval(13, 16)))
                                        .build())
                                .build()),
                arguments("Not syn query", "not(syn(tough))",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("1.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("syn(tough)", intervalsOf())
                                        .build())
                                .build()),
                arguments("Base levenshtein query", "levenshtein(thas,1)",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("2.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("levenshtein(thas,1)", intervalsOf(new Interval(0, 4)))
                                        .build())
                                .build()),
                arguments("Parenthesis levenshtein query", "((levenshtein(thas,1)))",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("2.txt").matches(new LinkedHashMap<>())
                                        .entry("levenshtein(thas,1)", intervalsOf(new Interval(0, 4)))
                                        .build())
                                .build()),
                arguments("Not levenshtein query", "not(levenshtein(thas,1))",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("1.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("levenshtein(thas,1)", intervalsOf())
                                        .build())
                                .build()),
                arguments("Complex query", "(lemm(he) or lemm(that)) and (syn(tough) or syn(supporter)) and not(lemm(absent)) and levenshtein(iq, 2)",
                        SearchResponse.builder()
                                .files(new ArrayList<>())
                                .file(FileMatch.builder()
                                        .file("1.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("lemm(he)", intervalsOf(new Interval(0, 2)))
                                        .entry("lemm(that)", intervalsOf())
                                        .entry("syn(tough)", intervalsOf())
                                        .entry("syn(supporter)", intervalsOf(new Interval(9, 15)))
                                        .entry("lemm(absent)", intervalsOf())
                                        .entry("levenshtein(iq,2)", intervalsOf(new Interval(0, 2), new Interval(3, 5), new Interval(6, 8)))
                                        .build())
                                .file(FileMatch.builder()
                                        .file("2.txt")
                                        .matches(new LinkedHashMap<>())
                                        .entry("lemm(he)", intervalsOf())
                                        .entry("lemm(that)", intervalsOf(new Interval(0, 4)))
                                        .entry("syn(tough)", intervalsOf(new Interval(13, 16)))
                                        .entry("syn(supporter)", intervalsOf())
                                        .entry("lemm(absent)", intervalsOf())
                                        .entry("levenshtein(iq,2)", intervalsOf(new Interval(5, 7)))
                                        .build()
                                )
                                .build())
        );
    }

    private static Set<Interval> intervalsOf(Interval... intervals) {
        return new LinkedHashSet<>(Arrays.asList(intervals));
    }
}