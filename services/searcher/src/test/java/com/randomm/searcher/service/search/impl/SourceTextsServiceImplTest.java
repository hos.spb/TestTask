package com.randomm.searcher.service.search.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SourceTextsServiceImplTest {

    private SourceTextsServiceImpl service;

    @BeforeEach
    public void setup() {
        service = new SourceTextsServiceImpl();
        service.setPathStatic("src/test/resources/sourceTexts");
    }

    @Test
    void getAllFilePathsReturnsFilePathsFromSpecifiedDirectory() {
        List<Path> expected = List.of(
                Paths.get("src/test/resources/sourceTexts/1.txt"),
                Paths.get("src/test/resources/sourceTexts/2.txt")
        );

        List<Path> actual = service.getAllFilePaths();

        assertThat(actual).hasSameElementsAs(expected);
    }
}