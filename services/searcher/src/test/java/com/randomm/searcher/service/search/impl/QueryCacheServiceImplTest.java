package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.antlr4.SearcherParser.LemmContext;
import com.randomm.searcher.antlr4.SearcherParser.LevenshteinContext;
import com.randomm.searcher.antlr4.SearcherParser.SynContext;
import com.randomm.searcher.service.nlp.LemmaService;
import com.randomm.searcher.service.nlp.SynonymService;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptySet;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

class QueryCacheServiceImplTest {


    private LemmaService lemmaService;
    private SynonymService synonymService;
    private Map<String, Set<String>> cache;

    private QueryCacheServiceImpl service;

    @BeforeEach
    void setUp() {
        lemmaService = mock(LemmaService.class);
        synonymService = mock(SynonymService.class);
        cache = mock(Map.class);

        service = new QueryCacheServiceImpl(lemmaService, synonymService);

        setField(service, "cache", cache);
    }

    @Test
    void getCache() {
        QueryCacheServiceImpl spyService = spy(service);
        ParseTree tree = mock(ParseTree.class);
        doNothing().when(spyService).visit(any());

        spyService.getCache(tree);
        //verifying test call because of spy
        verify(spyService).getCache(tree);

        verify(spyService).visit(tree);
        verifyNoMoreInteractions(spyService);
    }

    @Test
    void visitSynNotCachedYet() {
        final String query = "syn(test)";
        final String word = "test";
        final Set<String> expected = newHashSet("lem1", "lem2");

        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn(word);

        SynContext ctx = mock(SynContext.class);
        when(ctx.getText()).thenReturn(query);
        when(ctx.WORD()).thenReturn(node);

        when(synonymService.getSynonymsLemmas(any())).thenReturn(completedFuture(expected));
        when(cache.get(any())).thenReturn(null);

        service.visitSyn(ctx);

        verify(cache).get(query);
        verify(cache).put(query, expected);
        verify(synonymService).getSynonymsLemmas(word);
        verifyNoMoreInteractions(cache, lemmaService, synonymService);
    }

    @Test
    void visitSynAlreadyCached() {
        final String query = "lem(test)";

        SynContext ctx = mock(SynContext.class);
        when(ctx.getText()).thenReturn(query);

        when(cache.get(any())).thenReturn(emptySet());

        service.visitSyn(ctx);

        verify(cache).get(query);
        verifyNoMoreInteractions(cache, lemmaService, synonymService);
    }

    @Test
    void visitLemmNotCachedYet() {
        final String query = "lem(test)";
        final String word = "test";
        final Set<String> expected = newHashSet("lem1", "lem2");

        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn(word);

        LemmContext ctx = mock(LemmContext.class);
        when(ctx.getText()).thenReturn(query);
        when(ctx.WORD()).thenReturn(node);

        when(lemmaService.getLemmas(any())).thenReturn(completedFuture(expected));
        when(cache.get(any())).thenReturn(null);

        service.visitLemm(ctx);

        verify(cache).get(query);
        verify(cache).put(query, expected);
        verify(lemmaService).getLemmas(word);
        verifyNoMoreInteractions(cache, lemmaService, synonymService);
    }

    @Test
    void visitLemmAlreadyCached() {
        final String query = "lem(test)";

        LemmContext ctx = mock(LemmContext.class);
        when(ctx.getText()).thenReturn(query);

        when(cache.get(any())).thenReturn(emptySet());

        service.visitLemm(ctx);

        verify(cache).get(query);
        verifyNoMoreInteractions(cache, lemmaService, synonymService);
    }

    @Test
    void visitLevenshteinNotCachedYet() {
        final String query = "levenshtein(test,2)";

        LevenshteinContext ctx = mock(LevenshteinContext.class);
        when(ctx.getText()).thenReturn(query);

        when(cache.get(any())).thenReturn(null);

        service.visitLevenshtein(ctx);

        verify(cache).get(query);
        verify(cache).put(query, emptySet());
        verifyNoMoreInteractions(cache, lemmaService, synonymService);
    }

    @Test
    void visitLevenshteinAlreadyCached() {
        final String query = "levenshtein(test,2)";
        final Set<String> expected = emptySet();

        LevenshteinContext ctx = mock(LevenshteinContext.class);
        when(ctx.getText()).thenReturn(query);

        when(cache.get(any())).thenReturn(expected);

        service.visitLevenshtein(ctx);

        verify(cache).get(query);
        verifyNoMoreInteractions(cache, lemmaService, synonymService);
    }
}