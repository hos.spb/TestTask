package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.model.*;
import com.randomm.searcher.service.search.FileSearcherService;
import com.randomm.searcher.service.search.QueryCacheService;
import com.randomm.searcher.service.search.SourceTextsService;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionException;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.failedFuture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class SearchServiceImplTest {

    private SourceTextsService sourceTextsService;
    private FileSearcherService fileSearcherService;
    private QueryCacheService queryCacheService;
    private SearchServiceImpl service;

    @BeforeEach
    void setUp() {
        sourceTextsService = mock(SourceTextsService.class);
        fileSearcherService = mock(FileSearcherService.class);
        queryCacheService = mock(QueryCacheService.class);
        service = new SearchServiceImpl(sourceTextsService, fileSearcherService, queryCacheService);
    }

    @Test
    void searchThrowsExceptionForBadQuery() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> service.search("bad query"),
                "Expected search to throw, but it didn't"
        );

        assertThat(thrown.getMessage()).startsWith("Wrong query: ");
    }

    @Test
    void searchThrowsExceptionWhenExceptionThrownInFuture() {
        RuntimeException expectedException = new RuntimeException();
        when(sourceTextsService.getAllFilePaths()).thenReturn(List.of(mock(Path.class)));
        when(fileSearcherService.search(any(), any(), any())).thenReturn(failedFuture(expectedException));

        CompletionException thrown = assertThrows(
                CompletionException.class,
                () -> service.search("lemm(he)"),
                "Expected search to throw, but it didn't"
        );

        assertThat(thrown.getCause()).isEqualTo(expectedException);
    }

    @Test
    void searchReturnsOnlySuitedResult() {
        VisitorResult visitorResult = new VisitorResult();
        visitorResult.addMatch("query", Set.of(new Interval(1, 1)));
        FileResult suitedResult = new FileResult(true, "1.txt", visitorResult);
        FileResult notSuitedResult = new FileResult(false, "2.txt", null);
        final Path firstPath = mock(Path.class);
        final Path secondPath = mock(Path.class);

        FileMatch expectedMatch = new FileMatch("1.txt", Map.of("query", Set.of(new Interval(1, 1))));

        Map<String, Set<String>> cache = mock(Map.class);
        when(queryCacheService.getCache(any())).thenReturn(cache);
        when(sourceTextsService.getAllFilePaths()).thenReturn(List.of(firstPath, secondPath));
        when(fileSearcherService.search(any(), any(), any())).thenReturn(completedFuture(suitedResult), completedFuture(notSuitedResult));

        final SearchResponse actual = service.search("lemm(he)");

        verify(sourceTextsService).getAllFilePaths();
        verify(fileSearcherService).search(eq(firstPath), any(ParseTree.class), eq(cache));
        verify(fileSearcherService).search(eq(secondPath), any(ParseTree.class), eq(cache));
        verifyNoMoreInteractions(sourceTextsService, fileSearcherService);


        assertThat(actual.getFiles()).hasSize(1);
        assertThat(actual.getFiles().get(0)).isEqualTo(expectedMatch);
    }
}