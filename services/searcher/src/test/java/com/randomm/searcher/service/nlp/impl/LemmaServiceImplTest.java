package com.randomm.searcher.service.nlp.impl;

import com.randomm.proto.Lemmatizer.LemmaRequest;
import com.randomm.proto.Lemmatizer.LemmaResponse;
import com.randomm.proto.lemmatizerGrpc.lemmatizerFutureStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.google.common.util.concurrent.Futures.immediateFuture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@ExtendWith(MockitoExtension.class)
class LemmaServiceImplTest {

    private lemmatizerFutureStub stub;
    private LemmaServiceImpl service;

    @BeforeEach
    public void setup() {
        stub = mock(lemmatizerFutureStub.class);
        service = new LemmaServiceImpl();
        setField(service, "lemmatizer", stub);
    }

    @Test
    void getLemmasReturnExactlyWhatGotFromClient() throws ExecutionException, InterruptedException {
        LemmaResponse response = LemmaResponse.newBuilder().addLemmas("expected").build();
        when(stub.getLemmas(any())).thenReturn(immediateFuture(response));

        Set<String> actual = service.getLemmas("someWord").get();

        verify(stub).getLemmas(LemmaRequest.newBuilder().setWord("someWord").build());
        verifyNoMoreInteractions(stub);

        assertThat(actual).hasSize(1);
        assertThat(actual.iterator().next()).isEqualTo("expected");
    }
}