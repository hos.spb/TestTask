package com.randomm.searcher.reader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class CountingInputStreamTest {

    private InputStream in;
    private CountingInputStream stream;

    @BeforeEach
    public void setup() {
        in = mock(InputStream.class);
        stream = new CountingInputStream(in);
    }

    @Test
    void read() throws IOException {
        stream.read();
        assertThat(stream.getPrevCount()).isZero();

        stream.read();
        assertThat(stream.getPrevCount()).isEqualTo(1L);

        verify(in, times(2)).read();
        verifyNoMoreInteractions(in);
    }

    @Test
    void readWhenNoMoreData() throws IOException {
        when(in.read()).thenReturn(1, -1);

        stream.read();
        assertThat(stream.getPrevCount()).isZero();

        stream.read();
        assertThat(stream.getPrevCount()).isZero();

        verify(in, times(2)).read();
        verifyNoMoreInteractions(in);
    }

    @Test
    void testReadWithParams() throws IOException {
        byte[] b = new byte[0];
        int off = 100;
        int len = 10;
        int expected = 50;

        when(in.read(any(), anyInt(), anyInt())).thenReturn(expected);

        stream.read(b, off, len);
        assertThat(stream.getPrevCount()).isZero();

        stream.read(b, off, len);
        assertThat(stream.getPrevCount()).isEqualTo(expected);

        verify(in, times(2)).read(b, off, len);
        verifyNoMoreInteractions(in);
    }

    @Test
    void testReadWithParamsWhenNoMoreData() throws IOException {
        byte[] b = new byte[0];
        int off = 100;
        int len = 10;

        when(in.read(any(), anyInt(), anyInt())).thenReturn(len, -1);

        stream.read(b, off, len);
        assertThat(stream.getPrevCount()).isZero();

        stream.read(b, off, len);
        assertThat(stream.getPrevCount()).isZero();

        verify(in, times(2)).read(b, off, len);
        verifyNoMoreInteractions(in);
    }

    @Test
    void skip() throws IOException {
        long n = 100;
        long expected = 50;

        when(in.skip(anyLong())).thenReturn(expected);

        stream.skip(n);
        assertThat(stream.getPrevCount()).isZero();

        stream.skip(n);
        assertThat(stream.getPrevCount()).isEqualTo(expected);


        verify(in, times(2)).skip(n);
        verifyNoMoreInteractions(in);
    }

    @Test
    void skipWhenNoMoreData() throws IOException {
        long n = 100;
        long expected = 50;

        when(in.skip(anyLong())).thenReturn(expected, 0L);

        stream.skip(n);
        assertThat(stream.getPrevCount()).isZero();

        stream.skip(n);
        assertThat(stream.getPrevCount()).isEqualTo(expected);


        verify(in, times(2)).skip(n);
        verifyNoMoreInteractions(in);
    }
}