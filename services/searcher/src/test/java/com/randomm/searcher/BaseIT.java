package com.randomm.searcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.randomm.searcher.service.nlp.LemmaService;
import com.randomm.searcher.service.nlp.SynonymService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@ExtendWith(value = SpringExtension.class)
@SpringBootTest(classes = SearcherApplication.class)
@Disabled("Base class for integration tests")
public class BaseIT {

    @Autowired
    protected ObjectMapper mapper = new ObjectMapper();
    protected MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;
    @MockBean
    private LemmaService lemmaService;
    @MockBean
    private SynonymService synonymService;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();

        when(lemmaService.getLemmas(any())).thenAnswer(invocationOnMock -> completedFuture(newHashSet((String) invocationOnMock.getArgument(0))));
        when(synonymService.getSynonymsLemmas("tough")).thenReturn(completedFuture(newHashSet("bad")));
        when(synonymService.getSynonymsLemmas("supporter")).thenReturn(completedFuture(newHashSet("friend")));
    }
}
