package com.randomm.searcher.visitor;

import com.randomm.searcher.antlr4.SearcherParser.*;
import com.randomm.searcher.model.Interval;
import com.randomm.searcher.model.VisitorState;
import com.randomm.searcher.service.nlp.LemmaService;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

class QueryVisitorTest {

    private static final String testWord = "test";
    private static final Interval interval = new Interval(1, 1);

    private MockedStatic<LevenshteinDistance> ldStatic;
    public LemmaService lemmaService;
    public Map<String, Set<String>> queryFunctionCache;
    public VisitorState state;
    public QueryVisitor visitor;

    @BeforeEach
    public void setup() {
        ldStatic = mockStatic(LevenshteinDistance.class);

        state = mock(VisitorState.class);
        lemmaService = mock(LemmaService.class);
        queryFunctionCache = mock(Map.class);
        visitor = QueryVisitor.of(lemmaService, queryFunctionCache, testWord, interval);

        setField(visitor, "state", state);
    }

    @AfterEach
    public void closeStaticMock() {
        ldStatic.close();
    }

    @Test
    void visitParse() throws ExecutionException, InterruptedException {
        ParseContext ctx = mock(ParseContext.class);
        QueryVisitor spyVisitor = spy(visitor);
        ExprContext expectedContext = mock(ExprContext.class);

        when(ctx.expr()).thenReturn(expectedContext);
        doReturn(completedFuture(null)).when(spyVisitor).visit(any());

        spyVisitor.visitParse(ctx).get();
        //verifying test call because of spy
        verify(spyVisitor).visitParse(ctx);

        verify(ctx).expr();
        verify(spyVisitor).visit(expectedContext);
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(spyVisitor, ctx);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitExprPar() throws ExecutionException, InterruptedException {
        ExprParContext ctx = mock(ExprParContext.class);
        QueryVisitor spyVisitor = spy(visitor);
        ExprContext expectedContext = mock(ExprContext.class);

        when(ctx.expr()).thenReturn(expectedContext);
        doReturn(completedFuture(null)).when(spyVisitor).visit(any());

        spyVisitor.visitExprPar(ctx).get();
        //verifying test call because of spy
        verify(spyVisitor).visitExprPar(ctx);

        verify(ctx).expr();
        verify(spyVisitor).visit(expectedContext);
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(spyVisitor, ctx);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitFuncPar() throws ExecutionException, InterruptedException {
        FuncParContext ctx = mock(FuncParContext.class);
        QueryVisitor spyVisitor = spy(visitor);
        FunctionContext expectedContext = mock(FunctionContext.class);

        when(ctx.function()).thenReturn(expectedContext);
        doReturn(completedFuture(null)).when(spyVisitor).visit(any());

        spyVisitor.visitFuncPar(ctx).get();
        //verifying test call because of spy
        verify(spyVisitor).visitFuncPar(ctx);

        verify(ctx).function();
        verify(spyVisitor).visit(expectedContext);
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(spyVisitor, ctx);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitNot() throws ExecutionException, InterruptedException {
        NotContext ctx = mock(NotContext.class);
        QueryVisitor spyVisitor = spy(visitor);
        FunctionContext expectedContext = mock(FunctionContext.class);

        when(ctx.function()).thenReturn(expectedContext);
        doReturn(completedFuture(null)).when(spyVisitor).visit(any());

        spyVisitor.visitNot(ctx).get();
        //verifying test call because of spy
        verify(spyVisitor).visitNot(ctx);

        verify(ctx).function();
        verify(spyVisitor).visit(expectedContext);
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(spyVisitor, ctx);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitOr() throws ExecutionException, InterruptedException {
        ExprContext left = mock(ExprContext.class);
        ExprContext right = mock(ExprContext.class);

        OrContext ctx = new OrContext(mock(ExprContext.class));
        ctx.left = left;
        ctx.right = right;
        QueryVisitor spyVisitor = spy(visitor);

        doReturn(completedFuture(null)).when(spyVisitor).visit(any());

        spyVisitor.visitOr(ctx).get();
        //verifying test call because of spy
        verify(spyVisitor).visitOr(ctx);

        verify(spyVisitor).visit(left);
        verify(spyVisitor).visit(right);
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(spyVisitor);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitAnd() throws ExecutionException, InterruptedException {
        ExprContext left = mock(ExprContext.class);
        ExprContext right = mock(ExprContext.class);

        AndContext ctx = new AndContext(mock(ExprContext.class));
        ctx.left = left;
        ctx.right = right;
        QueryVisitor spyVisitor = spy(visitor);

        doReturn(completedFuture(null)).when(spyVisitor).visit(any());

        spyVisitor.visitAnd(ctx).get();
        //verifying test call because of spy
        verify(spyVisitor).visitAnd(ctx);

        verify(spyVisitor).visit(left);
        verify(spyVisitor).visit(right);
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(spyVisitor);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitSynNoCache() {
        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn("tst");

        SynContext ctx = mock(SynContext.class);
        when(ctx.WORD()).thenReturn(node);
        when(ctx.getText()).thenReturn("syn(tst)");

        when(queryFunctionCache.get(any())).thenReturn(null);

        ExecutionException thrown = assertThrows(
                ExecutionException.class,
                () -> visitor.visitSyn(ctx).get(),
                "Expected visitSyn to throw, but it didn't"
        );

        assertThat(thrown.getCause()).isInstanceOf(IllegalStateException.class);
        assertThat(thrown.getCause().getMessage()).isEqualTo("No cache of syn(tst) have been found");

        verify(queryFunctionCache).get("syn(tst)");
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(queryFunctionCache);
        verifyNoInteractions(lemmaService, state);
    }

    @Test
    void visitSynNoIntersections() throws ExecutionException, InterruptedException {
        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn("tst");

        SynContext ctx = mock(SynContext.class);
        when(ctx.WORD()).thenReturn(node);
        when(ctx.getText()).thenReturn("syn(tst)");

        when(queryFunctionCache.get(any())).thenReturn(newHashSet("syn1", "syn2"));
        when(lemmaService.getLemmas(any())).thenReturn(completedFuture(newHashSet("syn3", "syn4")));

        visitor.visitSyn(ctx).get();

        verify(queryFunctionCache).get("syn(tst)");
        verify(lemmaService).getLemmas(testWord);

        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(lemmaService, queryFunctionCache);
        verifyNoInteractions(state);
    }

    @Test
    void visitSyn() throws ExecutionException, InterruptedException {
        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn("tst");

        SynContext ctx = mock(SynContext.class);
        when(ctx.WORD()).thenReturn(node);
        when(ctx.getText()).thenReturn("syn(tst)");

        when(queryFunctionCache.get(any())).thenReturn(newHashSet("syn1", "syn2"));
        when(lemmaService.getLemmas(any())).thenReturn(completedFuture(newHashSet("syn2", "syn3")));

        visitor.visitSyn(ctx).get();

        verify(queryFunctionCache).get("syn(tst)");
        verify(lemmaService).getLemmas(testWord);
        verify(state).add(ctx, interval);

        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitLemmNoCache() {
        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn("tst");

        LemmContext ctx = mock(LemmContext.class);
        when(ctx.WORD()).thenReturn(node);
        when(ctx.getText()).thenReturn("lem(tst)");

        when(queryFunctionCache.get(any())).thenReturn(null);

        ExecutionException thrown = assertThrows(
                ExecutionException.class,
                () -> visitor.visitLemm(ctx).get(),
                "Expected visitSyn to throw, but it didn't"
        );

        assertThat(thrown.getCause()).isInstanceOf(IllegalStateException.class);
        assertThat(thrown.getCause().getMessage()).isEqualTo("No cache of lem(tst) have been found");

        verify(queryFunctionCache).get("lem(tst)");
        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(queryFunctionCache);
        verifyNoInteractions(lemmaService, state);
    }

    @Test
    void visitLemmNoIntersections() throws ExecutionException, InterruptedException {
        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn("tst");

        LemmContext ctx = mock(LemmContext.class);
        when(ctx.WORD()).thenReturn(node);
        when(ctx.getText()).thenReturn("lem(tst)");

        when(queryFunctionCache.get(any())).thenReturn(newHashSet("lem1", "lem2"));
        when(lemmaService.getLemmas(any())).thenReturn(completedFuture(newHashSet("lem3", "lem4")));

        visitor.visitLemm(ctx).get();

        verify(queryFunctionCache).get("lem(tst)");
        verify(lemmaService).getLemmas(testWord);

        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(lemmaService, queryFunctionCache);
        verifyNoInteractions(state);
    }

    @Test
    void visitLemm() throws ExecutionException, InterruptedException {
        TerminalNode node = mock(TerminalNode.class);
        when(node.getText()).thenReturn("tst");

        LemmContext ctx = mock(LemmContext.class);
        when(ctx.WORD()).thenReturn(node);
        when(ctx.getText()).thenReturn("lem(tst)");

        when(queryFunctionCache.get(any())).thenReturn(newHashSet("lem1", "lem2"));
        when(lemmaService.getLemmas(any())).thenReturn(completedFuture(newHashSet("lem2", "lem3")));

        visitor.visitLemm(ctx).get();

        verify(queryFunctionCache).get("lem(tst)");
        verify(lemmaService).getLemmas(testWord);
        verify(state).add(ctx, interval);

        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitLevenshteinNotSuited() throws ExecutionException, InterruptedException {
        TerminalNode wordNode = mock(TerminalNode.class);
        when(wordNode.getText()).thenReturn("tst");
        TerminalNode intNode = mock(TerminalNode.class);
        when(intNode.getText()).thenReturn("0");

        LevenshteinContext ctx = mock(LevenshteinContext.class);
        when(ctx.WORD()).thenReturn(wordNode);
        when(ctx.INT()).thenReturn(intNode);
        when(ctx.getText()).thenReturn("levenshtein(tst,0)");

        visitor.visitLevenshtein(ctx).get();


        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(state);
        verifyNoInteractions(lemmaService, queryFunctionCache, state);
    }

    @Test
    void visitLevenshteinSuited() throws ExecutionException, InterruptedException {
        TerminalNode wordNode = mock(TerminalNode.class);
        when(wordNode.getText()).thenReturn("tst");
        TerminalNode intNode = mock(TerminalNode.class);
        when(intNode.getText()).thenReturn("2");

        LevenshteinContext ctx = mock(LevenshteinContext.class);
        when(ctx.WORD()).thenReturn(wordNode);
        when(ctx.INT()).thenReturn(intNode);
        when(ctx.getText()).thenReturn("levenshtein(tst,2)");

        visitor.visitLevenshtein(ctx).get();

        verify(state).add(ctx, interval);

        ldStatic.verifyNoInteractions();
        verifyNoMoreInteractions(state);
        verifyNoInteractions(lemmaService, queryFunctionCache);
    }
}