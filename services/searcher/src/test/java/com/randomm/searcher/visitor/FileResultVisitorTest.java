package com.randomm.searcher.visitor;

import com.randomm.searcher.antlr4.SearcherParser.*;
import com.randomm.searcher.model.Interval;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class FileResultVisitorTest {

    public static final String file = "/folder/fileName.txt";
    public Map<String, Set<Interval>> matches;
    public ParseTree tree;
    public FileResultVisitor visitor;

    @BeforeEach
    public void setup() {
        matches = mock(Map.class);
        tree = mock(ParseTree.class);
        visitor = FileResultVisitor.of(matches, file, tree);
    }

    @ParameterizedTest
    @MethodSource("unaryProvider")
    void visitParse(boolean expected) {
        ParseContext ctx = mock(ParseContext.class);
        FileResultVisitor spyVisitor = spy(visitor);
        ExprContext expectedContext = mock(ExprContext.class);

        when(ctx.expr()).thenReturn(expectedContext);
        doReturn(expected).when(spyVisitor).visit(any());

        assertThat(spyVisitor.visitParse(ctx)).isEqualTo(expected);
        //verifying test call because of spy
        verify(spyVisitor).visitParse(ctx);

        verify(ctx).expr();
        verify(spyVisitor).visit(expectedContext);
        verifyNoMoreInteractions(spyVisitor, ctx);
    }

    @ParameterizedTest
    @MethodSource("unaryProvider")
    void visitExprPar(boolean expected) {
        ExprParContext ctx = mock(ExprParContext.class);
        FileResultVisitor spyVisitor = spy(visitor);
        ExprContext expectedContext = mock(ExprContext.class);

        when(ctx.expr()).thenReturn(expectedContext);
        doReturn(expected).when(spyVisitor).visit(any());

        assertThat(spyVisitor.visitExprPar(ctx)).isEqualTo(expected);
        //verifying test call because of spy
        verify(spyVisitor).visitExprPar(ctx);

        verify(ctx).expr();
        verify(spyVisitor).visit(expectedContext);
        verifyNoMoreInteractions(spyVisitor, ctx);
    }


    @ParameterizedTest
    @MethodSource("unaryProvider")
    void visitFuncPar(boolean expected) {
        FuncParContext ctx = mock(FuncParContext.class);
        FileResultVisitor spyVisitor = spy(visitor);
        FunctionContext expectedContext = mock(FunctionContext.class);

        when(ctx.function()).thenReturn(expectedContext);
        doReturn(expected).when(spyVisitor).visit(any());

        assertThat(spyVisitor.visitFuncPar(ctx)).isEqualTo(expected);
        //verifying test call because of spy
        verify(spyVisitor).visitFuncPar(ctx);

        verify(ctx).function();
        verify(spyVisitor).visit(expectedContext);
        verifyNoMoreInteractions(spyVisitor, ctx);
    }

    @ParameterizedTest
    @MethodSource("unaryProvider")
    void visitNot(boolean expected) {
        NotContext ctx = mock(NotContext.class);
        FileResultVisitor spyVisitor = spy(visitor);
        FunctionContext expectedContext = mock(FunctionContext.class);

        when(ctx.function()).thenReturn(expectedContext);
        doReturn(expected).when(spyVisitor).visit(any());

        assertThat(spyVisitor.visitNot(ctx)).isEqualTo(!expected);
        //verifying test call because of spy
        verify(spyVisitor).visitNot(ctx);

        verify(ctx).function();
        verify(spyVisitor).visit(expectedContext);
        verifyNoMoreInteractions(spyVisitor, ctx);
    }

    @ParameterizedTest
    @MethodSource("orProvider")
    void visitOr(boolean leftResult, boolean rightResult, boolean expected) {
        ExprContext left = mock(ExprContext.class);
        ExprContext right = mock(ExprContext.class);

        OrContext ctx = new OrContext(mock(ExprContext.class));
        ctx.left = left;
        ctx.right = right;
        FileResultVisitor spyVisitor = spy(visitor);

        doReturn(leftResult).when(spyVisitor).visit(left);
        doReturn(rightResult).when(spyVisitor).visit(right);

        assertThat(spyVisitor.visitOr(ctx)).isEqualTo(expected);
        //verifying test call because of spy
        verify(spyVisitor).visitOr(ctx);

        verify(spyVisitor, atMostOnce()).visit(left);
        verify(spyVisitor, atMostOnce()).visit(right);
        verifyNoMoreInteractions(spyVisitor);
    }

    @ParameterizedTest
    @MethodSource("andProvider")
    void visitAnd(boolean leftResult, boolean rightResult, boolean expected) {
        ExprContext left = mock(ExprContext.class);
        ExprContext right = mock(ExprContext.class);

        AndContext ctx = new AndContext(mock(ExprContext.class));
        ctx.left = left;
        ctx.right = right;
        FileResultVisitor spyVisitor = spy(visitor);

        doReturn(leftResult).when(spyVisitor).visit(left);
        doReturn(rightResult).when(spyVisitor).visit(right);

        assertThat(spyVisitor.visitAnd(ctx)).isEqualTo(expected);
        //verifying test call because of spy
        verify(spyVisitor).visitAnd(ctx);

        verify(spyVisitor, atMostOnce()).visit(left);
        verify(spyVisitor, atMostOnce()).visit(right);
        verifyNoMoreInteractions(spyVisitor);
    }

    @ParameterizedTest
    @MethodSource("setProvider")
    void visitSyn(Set<Interval> intervals, boolean expected) {
        SynContext ctx = mock(SynContext.class);
        when(ctx.getText()).thenReturn("syn(tst)");
        when(matches.get(any())).thenReturn(intervals);

        assertThat(visitor.visitSyn(ctx)).isEqualTo(expected);

        verify(ctx).getText();
        verify(matches).get("syn(tst)");

        verifyNoMoreInteractions(ctx, matches, tree);
    }

    @ParameterizedTest
    @MethodSource("setProvider")
    void visitLemm(Set<Interval> intervals, boolean expected) {
        LemmContext ctx = mock(LemmContext.class);
        when(ctx.getText()).thenReturn("lemm(tst)");
        when(matches.get(any())).thenReturn(intervals);

        assertThat(visitor.visitLemm(ctx)).isEqualTo(expected);

        verify(ctx).getText();
        verify(matches).get("lemm(tst)");

        verifyNoMoreInteractions(ctx, matches, tree);
    }


    @ParameterizedTest
    @MethodSource("setProvider")
    void visitLevenshtein(Set<Interval> intervals, boolean expected) {
        LevenshteinContext ctx = mock(LevenshteinContext.class);
        when(ctx.getText()).thenReturn("levenshtein(tst,2)");
        when(matches.get(any())).thenReturn(intervals);

        assertThat(visitor.visitLevenshtein(ctx)).isEqualTo(expected);

        verify(ctx).getText();
        verify(matches).get("levenshtein(tst,2)");

        verifyNoMoreInteractions(ctx, matches, tree);
    }

    private static Stream<Arguments> unaryProvider() {
        return Stream.of(
                arguments(false),
                arguments(true));
    }

    private static Stream<Arguments> orProvider() {
        return Stream.of(
                arguments(false, false, false),
                arguments(false, true, true),
                arguments(true, false, true),
                arguments(true, true, true));
    }

    private static Stream<Arguments> andProvider() {
        return Stream.of(
                arguments(false, false, false),
                arguments(false, true, false),
                arguments(true, false, false),
                arguments(true, true, true));
    }

    private static Stream<Arguments> setProvider() {
        return Stream.of(
                arguments(null, false),
                arguments(emptySet(), false),
                arguments(newHashSet(new Interval(1, 3)), true));
    }
}