package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.model.FileResult;
import com.randomm.searcher.model.Interval;
import com.randomm.searcher.model.VisitorResult;
import com.randomm.searcher.reader.FileWordReader;
import com.randomm.searcher.service.nlp.LemmaService;
import com.randomm.searcher.service.search.SourceTextsService;
import com.randomm.searcher.visitor.FileResultVisitor;
import com.randomm.searcher.visitor.QueryVisitor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;

import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyMap;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class FileSearcherServiceImplTest {

    private MockedStatic<QueryVisitor> visitorStatic;
    private MockedStatic<FileResultVisitor> fileVisitorStatic;

    private SourceTextsService sourceTextsService;
    private LemmaService lemmaService;
    private FileSearcherServiceImpl service;

    @BeforeEach
    public void setup() {
        visitorStatic = mockStatic(QueryVisitor.class);
        fileVisitorStatic = mockStatic(FileResultVisitor.class);

        lemmaService = mock(LemmaService.class);
        sourceTextsService = mock(SourceTextsService.class);
        service = new FileSearcherServiceImpl(lemmaService, sourceTextsService);
    }

    @AfterEach
    public void closeStaticMock() {
        visitorStatic.close();
        fileVisitorStatic.close();
    }

    @Test
    void visitCloseReaderEvenIfExceptionThrown() {
        FileWordReader reader = mock(FileWordReader.class);
        when(reader.hasNext()).thenReturn(true, false);
        when(reader.nextWord()).thenThrow(new RuntimeException());

        when(sourceTextsService.getReader(any())).thenReturn(reader);

        assertThrows(
                RuntimeException.class,
                () -> service.search(mock(Path.class), mock(ParseTree.class), emptyMap()),
                "Expected visit to throw, but it didn't"
        );

        verify(reader).close();
    }

    @Test
    void visit() throws ExecutionException, InterruptedException {
        ParseTree tree = mock(ParseTree.class);
        Path path = mock(Path.class);

        FileWordReader reader = mock(FileWordReader.class);
        when(reader.hasNext()).thenReturn(true, true, false);
        when(reader.nextWord()).thenReturn("firstWord", "secondWord");
        when(reader.getInterval()).thenReturn(new Interval(1, 1), new Interval(2, 2));
        when(reader.getRelativeFilePath()).thenReturn("tst.txt");

        QueryVisitor firstWordVisitor = mock(QueryVisitor.class);
        VisitorResult firstResult = new VisitorResult();
        firstResult.addMatch("query", newHashSet(new Interval(1, 1)));
        when(firstWordVisitor.visitTree(any())).thenReturn(completedFuture(firstResult));

        QueryVisitor secondWordVisitor = mock(QueryVisitor.class);
        VisitorResult secondResult = new VisitorResult();
        secondResult.addMatch("query", newHashSet(new Interval(2, 2)));
        when(secondWordVisitor.visitTree(any())).thenReturn(completedFuture(secondResult));

        when(QueryVisitor.of(any(), any(), any(), any())).thenReturn(firstWordVisitor, secondWordVisitor);
        when(sourceTextsService.getReader(any())).thenReturn(reader);

        FileResultVisitor fileResultVisitor = mock(FileResultVisitor.class);
        FileResult expected = mock(FileResult.class);
        when(FileResultVisitor.of(any(), any(), any())).thenReturn(fileResultVisitor);
        when(fileResultVisitor.getResult()).thenReturn(expected);

        FileResult actual = service.search(path, tree, emptyMap()).get();

        ArgumentCaptor<String> wordCapture = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Interval> intervalCapture = ArgumentCaptor.forClass(Interval.class);
        visitorStatic.verify(() -> QueryVisitor.of(eq(lemmaService), eq(emptyMap()), wordCapture.capture(), intervalCapture.capture()), times(2));
        fileVisitorStatic.verify(() -> FileResultVisitor.of(Map.of("query", newHashSet(new Interval(1, 1), new Interval(2, 2))), "tst.txt", tree));
        verify(sourceTextsService).getReader(path);

        verify(firstWordVisitor).visitTree(tree);
        verify(secondWordVisitor).visitTree(tree);

        verify(reader, times(3)).hasNext();
        verify(reader, times(2)).nextWord();
        verify(reader, times(2)).getInterval();
        verify(reader).getRelativeFilePath();
        verify(reader).close();

        assertThat(actual).isEqualTo(expected);
        assertThat(wordCapture.getAllValues()).hasSameElementsAs(newHashSet("firstword", "secondword"));
        assertThat(intervalCapture.getAllValues()).hasSameElementsAs(newHashSet(new Interval(1, 1), new Interval(2, 2)));

        visitorStatic.verifyNoMoreInteractions();
        verifyNoMoreInteractions(secondWordVisitor, reader, lemmaService, sourceTextsService);
    }
}