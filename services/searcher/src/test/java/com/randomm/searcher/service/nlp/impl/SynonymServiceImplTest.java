package com.randomm.searcher.service.nlp.impl;

import com.randomm.proto.synonymizerGrpc.synonymizerFutureStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.google.common.util.concurrent.Futures.immediateFuture;
import static com.randomm.proto.Synonymizer.SynonymRequest;
import static com.randomm.proto.Synonymizer.SynonymResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@ExtendWith(MockitoExtension.class)
class SynonymServiceImplTest {

    private synonymizerFutureStub stub;
    private SynonymServiceImpl service;

    @BeforeEach
    public void setup() {
        stub = mock(synonymizerFutureStub.class);
        service = new SynonymServiceImpl();
        setField(service, "synonymizer", stub);
    }

    @Test
    void getLemmasReturnExactlyWhatGotFromClient() throws ExecutionException, InterruptedException {
        SynonymResponse response = SynonymResponse.newBuilder().addSynonyms("expected").build();
        when(stub.getSynonymsLemmas(any())).thenReturn(immediateFuture(response));

        Set<String> actual = service.getSynonymsLemmas("someWord").get();

        verify(stub).getSynonymsLemmas(SynonymRequest.newBuilder().setWord("someWord").build());
        verifyNoMoreInteractions(stub);

        assertThat(actual).hasSize(1);
        assertThat(actual.iterator().next()).isEqualTo("expected");
    }
}