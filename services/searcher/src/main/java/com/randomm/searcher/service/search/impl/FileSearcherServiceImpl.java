package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.model.FileResult;
import com.randomm.searcher.model.Interval;
import com.randomm.searcher.model.VisitorResult;
import com.randomm.searcher.reader.FileWordReader;
import com.randomm.searcher.service.nlp.LemmaService;
import com.randomm.searcher.service.search.FileSearcherService;
import com.randomm.searcher.service.search.SourceTextsService;
import com.randomm.searcher.visitor.FileResultVisitor;
import com.randomm.searcher.visitor.QueryVisitor;
import lombok.RequiredArgsConstructor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toMap;

@Service
@RequiredArgsConstructor
public class FileSearcherServiceImpl implements FileSearcherService {

    private final LemmaService lemmaService;
    private final SourceTextsService sourceTextsService;

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletableFuture<FileResult> search(@NonNull Path path, @NonNull ParseTree tree, Map<String, Set<String>> queryFunctionCache) {
        List<CompletableFuture<VisitorResult>> futureResults = new ArrayList<>();
        FileWordReader reader = sourceTextsService.getReader(path);

        try (reader) {
            while (reader.hasNext()) {
                final String testWord = reader.nextWord().toLowerCase();
                final Interval interval = reader.getInterval();
                QueryVisitor visitor = QueryVisitor.of(lemmaService, queryFunctionCache, testWord, interval);

                futureResults.add(visitor.visitTree(tree));
            }
        }

        return CompletableFuture.allOf(futureResults.toArray(new CompletableFuture[0]))
                .thenApply(avoid -> futureResults.stream()
                        .map(CompletableFuture::join)
                        .map(VisitorResult::getMatches)
                        .map(Map::entrySet)
                        .flatMap(Collection::stream)
                        .collect(collectingAndThen(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (left, right) -> {
                                    left.addAll(right);
                                    return left;
                                }, LinkedHashMap::new), map -> FileResultVisitor.of(map, reader.getRelativeFilePath(), tree).getResult())));
    }

}
