package com.randomm.searcher.reader;

import org.springframework.lang.NonNull;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * InputStream wrapper with read bytes counting
 */
public class CountingInputStream extends FilterInputStream {

    /**
     * Amount of read data in bytes
     */
    private long count;

    /**
     * Amount of read data in bytes, before last read operation
     */
    private long prevCount;

    public CountingInputStream(@NonNull InputStream in) {
        super(in);
    }

    public long getPrevCount() {
        return prevCount;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Increments counter after each {@link InputStream#read() read} call
     */
    @Override
    public int read() throws IOException {
        int result = super.read();
        if (result != -1) {
            prevCount = count;
            count++;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Increments counter after each {@link InputStream#read(byte[], int, int) read} call
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int result = super.read(b, off, len);
        if (result != -1) {
            prevCount = count;
            count += result;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Increments counter after each {@link InputStream#skip(long) skip} call
     */
    @Override
    public long skip(long n) throws IOException {
        long result = super.skip(n);
        prevCount = count;
        count += result;
        return result;
    }
}
