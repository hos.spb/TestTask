package com.randomm.searcher.config;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Executor configuration
 */
@Configuration
@EnableAsync
@ConfigurationProperties(prefix = "executor")
@Setter
public class ExecutorConfig {

    /**
     * Amount of threads initialized by starting the application. <br>
     * Use the following property to change value: {@code executor.corePoolSize}
     */
    private int corePoolSize;

    /**
     * Pool size upper bound.<br>
     * Use the following property to change value: {@code executor.maxPoolSize}
     */
    private int maxPoolSize;

    /**
     * {@link Executor} bean for async operation execution.<br>
     * See also: {@link #corePoolSize corePoolSize}, {@link #maxPoolSize maxPoolSize}
     *
     * @return instance of {@link Executor}
     */
    @Bean
    public Executor fileVisitorTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.initialize();
        return executor;
    }
}
