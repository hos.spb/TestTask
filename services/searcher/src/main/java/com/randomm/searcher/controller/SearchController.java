package com.randomm.searcher.controller;

import com.randomm.searcher.model.SearchResponse;
import com.randomm.searcher.service.search.SearchService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Web-mvc controller for search operations.
 */
@RestController
@RequiredArgsConstructor
public class SearchController implements WebMvcConfigurer {

    private final SearchService searchService;

    /**
     * Search among the source texts according to the specified query.
     * For knowledge about query see at the service README.md
     *
     * @param query http query parameter which contains search query
     * @return {@link SearchResponse}
     */
    @Operation(summary = "Search among the source texts according to the specified query.")
    @GetMapping("/search")
    public ResponseEntity<SearchResponse> search(@Parameter(description = "Query for searching") @RequestParam("query") String query) {
        return ResponseEntity.ok(searchService.search(query));
    }
}
