package com.randomm.searcher.model;

import com.randomm.searcher.visitor.FileResultVisitor;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Model for result of tree traversal across the expression
 *
 * @see FileResultVisitor
 */
@Data
@AllArgsConstructor
public class FileResult {

    /**
     * The flag will contain true if file content satisfies provided condition, otherwise false.
     */
    private boolean suited;

    /**
     * File name with relative path
     */
    private String file;

    /**
     * {@link VisitorResult}
     */
    private VisitorResult visitorResult;
}
