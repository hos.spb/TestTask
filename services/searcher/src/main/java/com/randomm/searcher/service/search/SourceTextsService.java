package com.randomm.searcher.service.search;

import com.randomm.searcher.reader.FileWordReader;

import java.nio.file.Path;
import java.util.List;

/**
 * Service for work with source text files
 */
public interface SourceTextsService {

    /**
     * Getting all source text files, include subfolders
     *
     * @return {@link List} of file relative paths
     * @throws IllegalStateException if there was a problem with IO(like no specified folder, security etc.)
     */
    List<Path> getAllFilePaths();

    /**
     * Creates reader instance for reading file
     *
     * @param path path to file which need to be read
     * @return {@link FileWordReader}
     */
    FileWordReader getReader(Path path);
}
