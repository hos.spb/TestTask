package com.randomm.searcher.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.Map;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Found file data model")
public class FileMatch {

    /**
     * File name with relative path
     */
    @Schema(description = "File name with relative path")
    private String file;

    /**
     * The field contains a {@link Map map} with function as a key and set of intervals as a value.
     *
     * @see "resources/antlr4/Searcher.g4"
     */
    @Schema(description = "All query function results. Represents like function-intervals objects")
    @Singular("entry")
    private Map<String, Set<Interval>> matches;

    public FileMatch(FileResult fileResult) {
        this(fileResult.getFile(), fileResult.getVisitorResult().getMatches());
    }
}
