package com.randomm.searcher.service.nlp;

import org.springframework.lang.NonNull;
import org.springframework.scheduling.annotation.Async;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * Service for interacting with lemmas service
 */
public interface LemmaService {

    /**
     * Method find lemmas by word.
     *
     * @param word for lemmas searching
     * @return {@link CompletableFuture} with set of lemmas of provided word
     */
    @Async
    CompletableFuture<Set<String>> getLemmas(@NonNull String word);
}
