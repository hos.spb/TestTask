package com.randomm.searcher.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import static java.util.Collections.singletonMap;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * Main controller advice for exception handling. Also see {@link ControllerAdvice}, {@link ExceptionHandler}
 */
@ControllerAdvice
@Slf4j
public class WebExceptionHandler {

    @ExceptionHandler(value = {IllegalArgumentException.class, HttpClientErrorException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException e) {
        return error(e.getMessage(), HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {
        return error(e.getMessage(), e);
    }

    private ResponseEntity<Object> error(String message, Exception e) {
        return error(message, HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    private ResponseEntity<Object> error(String message, HttpStatus httpStatus, Exception e) {
        log.error(message, e);
        return ResponseEntity
                .status(httpStatus)
                .contentType(APPLICATION_JSON)
                .body(singletonMap("errorMessage", message));
    }
}

