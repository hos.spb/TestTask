package com.randomm.searcher.visitor;

import com.randomm.searcher.antlr4.SearcherBaseVisitor;
import com.randomm.searcher.model.FileResult;
import com.randomm.searcher.model.Interval;
import com.randomm.searcher.model.VisitorResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.Map;
import java.util.Set;

import static com.randomm.searcher.antlr4.SearcherParser.*;

/**
 * Antlr4 Visitor realization for compute result of file search <br>
 * All overridden  methods implements antlr4 grammar rules
 *
 * @see "resources/antlr4/Searcher.g4"
 */
@Slf4j
@RequiredArgsConstructor(staticName = "of")
public class FileResultVisitor extends SearcherBaseVisitor<Boolean> {

    /**
     * {@link Map map} with function as a key and set of found for function intervals as a value
     */
    private final Map<String, Set<Interval>> matches;

    /**
     * Filename with relative path
     */
    private final String file;

    /**
     * Antlr4 ParseTree of query
     *
     * @see ParseTree
     */
    private final ParseTree tree;

    /**
     * Get result of file visit
     *
     * @return {@link FileResult}
     * @see FileResult
     */
    public FileResult getResult() {
        return new FileResult(this.visit(tree), file, new VisitorResult(matches));
    }

    @Override
    public Boolean visitParse(ParseContext ctx) {
        return this.visit(ctx.expr());
    }

    @Override
    public Boolean visitExprPar(ExprParContext ctx) {
        return this.visit(ctx.expr());
    }

    @Override
    public Boolean visitFuncPar(FuncParContext ctx) {
        return this.visit(ctx.function());
    }

    @Override
    public Boolean visitNot(NotContext ctx) {
        return !this.visit(ctx.function());
    }

    @Override
    public Boolean visitOr(OrContext ctx) {
        return this.visit(ctx.left) || this.visit(ctx.right);
    }

    @Override
    public Boolean visitAnd(AndContext ctx) {
        return this.visit(ctx.left) && this.visit(ctx.right);
    }

    @Override
    public Boolean visitSyn(SynContext ctx) {
        return containsNotEmptyIntervals(ctx.getText());
    }

    @Override
    public Boolean visitLemm(LemmContext ctx) {
        return containsNotEmptyIntervals(ctx.getText());
    }

    @Override
    public Boolean visitLevenshtein(LevenshteinContext ctx) {
        return containsNotEmptyIntervals(ctx.getText());
    }

    private boolean containsNotEmptyIntervals(String query) {
        final Set<Interval> intervals = matches.get(query);
        return intervals != null && !intervals.isEmpty();
    }
}