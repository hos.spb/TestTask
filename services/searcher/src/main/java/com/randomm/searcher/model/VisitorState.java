package com.randomm.searcher.model;

import org.antlr.v4.runtime.RuleContext;
import org.springframework.lang.NonNull;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Thread-safe data model of visitor state.
 *
 * @see com.randomm.searcher.visitor.QueryVisitor
 */
public class VisitorState {

    /**
     * The field contains a {@link Map map} with function as a key and set of intervals as a value.
     */
    private final Map<String, Set<Interval>> state = new ConcurrentHashMap<>();

    /**
     * Add interval to state by context text
     * If set doesn't exist creates it
     */
    public void add(@NonNull RuleContext ctx, @NonNull Interval interval) {
        state.computeIfAbsent(ctx.getText(), k -> new ConcurrentSkipListSet<>())
                .add(interval);
    }

    public Set<Interval> get(@NonNull String query) {
        return state.get(query);
    }
}
