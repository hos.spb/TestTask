package com.randomm.searcher.service.search;

import org.antlr.v4.runtime.tree.ParseTree;

import java.util.Map;
import java.util.Set;

/**
 * Service for working with query expressions cache
 */
public interface QueryCacheService {

    /**
     * Returns cache for all functions from query which returns string set
     *
     * @param tree Antlr4 query tree
     * @return {@link Map map} with function as a key and set of strings as a value
     */
    Map<String, Set<String>> getCache(ParseTree tree);
}
