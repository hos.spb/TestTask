package com.randomm.searcher.reader;

import com.randomm.searcher.model.Interval;
import lombok.Getter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import static java.lang.String.format;

/**
 * The reader can read file by words. Supports only English words.
 */
public class FileWordReader implements AutoCloseable {
    /**
     * Delimiter - any character except lowercase and uppercase english letters.
     */
    private static final Pattern DELIMITERS = Pattern.compile("[^a-zA-Z]+");

    @Getter
    private final String relativeFilePath;
    private final Scanner scanner;
    private final CountingInputStream countingInputStream;

    /**
     * @param filePath path to target file
     */
    private FileWordReader(Path basePath, Path filePath) {
        try {
            countingInputStream = new CountingInputStream(new FileInputStream(filePath.toFile()));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(format("Exception thrown while trying to read file: %s", filePath), e);
        }

        scanner = new Scanner(countingInputStream);
        scanner.useDelimiter(DELIMITERS);
        this.relativeFilePath = basePath.toUri().relativize(filePath.toUri()).toString();
    }

    public static FileWordReader of(Path basePath, Path filePath) {
        return new FileWordReader(basePath, filePath);
    }

    /**
     * @return next word after {@link #DELIMITERS}
     * @see Scanner#next()
     */
    public String nextWord() {
        return scanner.next();
    }

    /**
     * @return true if stream contains next word after {@link #DELIMITERS}
     * @see Scanner#hasNext()
     */
    public boolean hasNext() {
        return scanner.hasNext();
    }

    /**
     * @return an interval of last read word
     */
    public Interval getInterval() {
        final MatchResult match = scanner.match();
        long count = countingInputStream.getPrevCount();

        return new Interval(count + match.start(), count + match.end());
    }

    /**
     * {@link Scanner#close()}
     */
    public void close() {
        scanner.close();
    }

}
