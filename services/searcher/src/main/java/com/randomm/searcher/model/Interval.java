package com.randomm.searcher.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Interval data model")
public class Interval implements Comparable<Interval> {

    @Schema(description = "contains beginning index (inclusive)")
    private long start;

    @Schema(description = "contains last index(exclusive)")
    private long end;

    @Override
    public int compareTo(Interval o) {
        return Long.compare(this.start, o.start);
    }
}
