package com.randomm.searcher.service.search;

import com.randomm.searcher.model.SearchResponse;

/**
 * Service for searching suitable files among the source texts.
 */
public interface SearchService {

    /**
     * Search by a given query
     *
     * @param query search query
     * @return {@link SearchResponse}
     * @throws IllegalArgumentException if query is wrong
     */
    SearchResponse search(String query);
}
