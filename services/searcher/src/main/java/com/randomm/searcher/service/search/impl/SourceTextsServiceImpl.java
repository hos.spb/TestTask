package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.reader.FileWordReader;
import com.randomm.searcher.service.search.SourceTextsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class SourceTextsServiceImpl implements SourceTextsService {

    /**
     * Source text folder path (relative to project root)
     */
    private static Path PATH;

    @Value("${sourceTextsPath}")
    public void setPathStatic(String path) {
        SourceTextsServiceImpl.PATH = Paths.get(path);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Path> getAllFilePaths() {
        try (Stream<Path> pathStream = Files.walk(PATH)) {
            List<Path> paths = pathStream
                    .filter(Files::isRegularFile)
                    .collect(toList());

            log.info(format("Detected source texts: %s", paths));

            return paths;
        } catch (IOException e) {
            throw new IllegalStateException(format("Exception thrown while trying to read files in directory: %s", PATH), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FileWordReader getReader(Path path) {
        return FileWordReader.of(PATH, path);
    }
}
