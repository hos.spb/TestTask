package com.randomm.searcher.visitor;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.ArrayList;
import java.util.List;

/**
 * Antlr4 Listener. Saves eny syntax error founded by {@link org.antlr.v4.runtime.Parser}
 *
 * @see BaseErrorListener
 */
public class SyntaxErrorListener extends BaseErrorListener {
    private final List<String> syntaxErrors = new ArrayList<>();

    public List<String> getSyntaxErrors() {
        return syntaxErrors;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        syntaxErrors.add(msg);
    }
}