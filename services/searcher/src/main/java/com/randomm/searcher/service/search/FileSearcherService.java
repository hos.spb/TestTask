package com.randomm.searcher.service.search;

import com.randomm.searcher.model.FileResult;
import org.antlr.v4.runtime.tree.ParseTree;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.annotation.Async;

import java.nio.file.Path;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * Service for searching results within a file
 */
public interface FileSearcherService {

    /**
     * Searching results within a file according to the rule
     *
     * @param path               path to file for search
     * @param tree               antlr4 ParseTree
     * @param queryFunctionCache pre-calculated results of functions in query
     * @return {@link FileResult}
     * @see ParseTree
     */
    @Async
    CompletableFuture<FileResult> search(@NonNull Path path, @NonNull ParseTree tree, Map<String, Set<String>> queryFunctionCache);
}
