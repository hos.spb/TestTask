package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.antlr4.SearcherLexer;
import com.randomm.searcher.antlr4.SearcherParser;
import com.randomm.searcher.model.FileMatch;
import com.randomm.searcher.model.FileResult;
import com.randomm.searcher.model.SearchResponse;
import com.randomm.searcher.service.search.FileSearcherService;
import com.randomm.searcher.service.search.QueryCacheService;
import com.randomm.searcher.service.search.SearchService;
import com.randomm.searcher.service.search.SourceTextsService;
import com.randomm.searcher.visitor.SyntaxErrorListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static java.lang.String.format;
import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final SourceTextsService sourceTextsService;
    private final FileSearcherService fileSearcherService;
    private final QueryCacheService queryCacheService;

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResponse search(@NonNull String query) {
        SearcherLexer lexer = new SearcherLexer(CharStreams.fromString(query.toLowerCase()));
        SearcherParser parser = new SearcherParser(new CommonTokenStream(lexer));
        SyntaxErrorListener listener = new SyntaxErrorListener();

        parser.addErrorListener(listener);
        ParseTree tree = parser.parse();

        if (parser.getNumberOfSyntaxErrors() != 0) {
            throw new IllegalArgumentException(format("Wrong query: %s", listener.getSyntaxErrors()));
        }

        Map<String, Set<String>> cache = queryCacheService.getCache(tree);

        List<CompletableFuture<FileResult>> futures = sourceTextsService.getAllFilePaths()
                .stream()
                .map(s -> fileSearcherService.search(s, tree, cache))
                .collect(toList());

        SearchResponse result = allOf(futures.toArray(new CompletableFuture[0]))
                .thenApply(avoid -> futures.stream()
                        .map(CompletableFuture::join)
                        .filter(FileResult::isSuited)
                        .map(FileMatch::new)
                        .collect(collectingAndThen(toList(), SearchResponse::new))).join();

        log.info(format("Results: %s", result));

        return result;
    }
}
