package com.randomm.searcher.service.search.impl;

import com.randomm.searcher.antlr4.SearcherBaseVisitor;
import com.randomm.searcher.antlr4.SearcherParser.LemmContext;
import com.randomm.searcher.service.nlp.LemmaService;
import com.randomm.searcher.service.nlp.SynonymService;
import com.randomm.searcher.service.search.QueryCacheService;
import lombok.RequiredArgsConstructor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static com.randomm.searcher.antlr4.SearcherParser.LevenshteinContext;
import static com.randomm.searcher.antlr4.SearcherParser.SynContext;

/**
 * All overridden methods implements antlr4 grammar rules
 *
 * @see "resources/antlr4/Searcher.g4"
 */
@Service
@RequiredArgsConstructor
public class QueryCacheServiceImpl extends SearcherBaseVisitor<Void> implements QueryCacheService {

    private final LemmaService lemmaService;
    private final SynonymService synonymService;

    /**
     * buffer variables used for save antlr4 visit method results
     */
    private Map<String, Set<String>> cache;

    /**
     * {@inheritDoc}
     */
    public Map<String, Set<String>> getCache(ParseTree tree) {
        cache = new LinkedHashMap<>();
        this.visit(tree);
        return cache;
    }

    @Override
    public Void visitSyn(SynContext ctx) {
        Set<String> querySynLemmas = cache.get(ctx.getText());

        if (querySynLemmas == null) {
            querySynLemmas = synonymService.getSynonymsLemmas(ctx.WORD().getText()).join();
            cache.put(ctx.getText(), querySynLemmas);
        }
        return null;
    }

    @Override
    public Void visitLemm(LemmContext ctx) {
        Set<String> queryLemmas = cache.get(ctx.getText());

        if (queryLemmas == null) {
            queryLemmas = lemmaService.getLemmas(ctx.WORD().getText()).join();
            cache.put(ctx.getText(), queryLemmas);
        }
        return null;
    }

    @Override
    public Void visitLevenshtein(LevenshteinContext ctx) {
        Set<String> queryLemmas = cache.get(ctx.getText());

        if (queryLemmas == null) {
            cache.put(ctx.getText(), new HashSet<>());
        }
        return null;
    }
}
