package com.randomm.searcher.visitor;

import com.randomm.searcher.antlr4.SearcherBaseVisitor;
import com.randomm.searcher.model.Interval;
import com.randomm.searcher.model.VisitorResult;
import com.randomm.searcher.model.VisitorState;
import com.randomm.searcher.service.nlp.LemmaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static com.randomm.searcher.antlr4.SearcherParser.*;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.CompletableFuture.*;

/**
 * Antlr4 Visitor realization for find all positive functions results of testWord<br>
 * All overridden methods implements antlr4 grammar rules
 *
 * @see "resources/antlr4/Searcher.g4"
 */
@Slf4j
@RequiredArgsConstructor(staticName = "of")
public class QueryVisitor extends SearcherBaseVisitor<CompletableFuture<Void>> {

    private final LemmaService lemmaService;

    /**
     * Thread-safe visitor state which contains all positive functions intervals
     */
    private final VisitorState state = new VisitorState();

    /**
     * Pre-calculated results of functions in query
     */
    private final Map<String, Set<String>> queryFunctionCache;

    /**
     * Word checked by an expression
     */
    private final String testWord;

    /**
     * Interval of testWord
     */
    private final Interval interval;

    /**
     * Visits the tree provided by Antlr4 saving all positive function results of testWord
     *
     * @param tree Antlr4 ParseTree
     * @return {@link VisitorResult}}
     * @see "resources/antlr4/Searcher.g4"
     */
    public CompletableFuture<VisitorResult> visitTree(ParseTree tree) {
        return this.visit(tree)
                .thenApply(visitorResult -> {
                    VisitorResult result = new VisitorResult();
                    queryFunctionCache.keySet().forEach(s -> result.addMatch(s, ofNullable(state.get(s)).orElse(new HashSet<>())));
                    return result;
                });
    }

    @Override
    public CompletableFuture<Void> visitParse(ParseContext ctx) {
        return this.visit(ctx.expr());
    }

    @Override
    public CompletableFuture<Void> visitExprPar(ExprParContext ctx) {
        return this.visit(ctx.expr());
    }

    @Override
    public CompletableFuture<Void> visitFuncPar(FuncParContext ctx) {
        return this.visit(ctx.function());
    }

    @Override
    public CompletableFuture<Void> visitNot(NotContext ctx) {
        return this.visit(ctx.function());
    }

    @Override
    public CompletableFuture<Void> visitOr(OrContext ctx) {
        return allOf(this.visit(ctx.left), this.visit(ctx.right));
    }

    @Override
    public CompletableFuture<Void> visitAnd(AndContext ctx) {
        return allOf(this.visit(ctx.left), this.visit(ctx.right));
    }

    @Override
    public CompletableFuture<Void> visitSyn(SynContext ctx) {
        return getQueryLemmas(ctx)
                .thenCompose(this::retainWithTestWordLemmas)
                .thenApply(wordLemmas -> addPositiveResultToState(ctx, !wordLemmas.isEmpty()));
    }

    @Override
    public CompletableFuture<Void> visitLemm(LemmContext ctx) {
        return getQueryLemmas(ctx)
                .thenCompose(this::retainWithTestWordLemmas)
                .thenApply(wordLemmas -> addPositiveResultToState(ctx, !wordLemmas.isEmpty()));
    }

    @Override
    public CompletableFuture<Void> visitLevenshtein(LevenshteinContext ctx) {
        return supplyAsync(() -> LevenshteinDistance.getDefaultInstance().apply(ctx.WORD().getText(), testWord) <= parseInt(ctx.INT().getText()))
                .thenApply(b -> addPositiveResultToState(ctx, b));
    }

    private CompletableFuture<Set<String>> getQueryLemmas(RuleContext ctx) {
        final String text = ctx.getText();
        Set<String> cashedLemmas = queryFunctionCache.get(text);

        return cashedLemmas == null
                ? failedFuture(new IllegalStateException(format("No cache of %s have been found", text)))
                : completedFuture(cashedLemmas);
    }

    private CompletableFuture<Set<String>> retainWithTestWordLemmas(Set<String> queryLemmas) {
        return lemmaService.getLemmas(testWord)
                .thenApply(wordLemmas -> {
                    wordLemmas.retainAll(queryLemmas);
                    return wordLemmas;
                });
    }

    private Void addPositiveResultToState(RuleContext ctx, boolean result) {
        if (result) {
            state.add(ctx, interval);
        }
        return null;
    }
}