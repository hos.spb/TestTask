package com.randomm.searcher.service.nlp.impl;

import com.randomm.proto.Synonymizer.SynonymRequest;
import com.randomm.proto.Synonymizer.SynonymResponse;
import com.randomm.proto.synonymizerGrpc;
import com.randomm.proto.synonymizerGrpc.synonymizerFutureStub;
import com.randomm.searcher.service.nlp.SynonymService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static net.javacrumbs.futureconverter.java8guava.FutureConverter.toCompletableFuture;

@Service
@RequiredArgsConstructor
public class SynonymServiceImpl implements SynonymService {

    @Value("${grpc.client.synonymizer.host}")
    private String host;
    @Value("${grpc.client.synonymizer.port}")
    private int port;

    private synonymizerFutureStub synonymizer;

    /**
     * Spring post init method
     * Initialize grpc client
     *
     * @see PostConstruct
     */
    @PostConstruct
    private void setup() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .defaultLoadBalancingPolicy("round_robin")
                .usePlaintext()
                .build();
        synonymizer = synonymizerGrpc.newFutureStub(channel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletableFuture<Set<String>> getSynonymsLemmas(@NonNull String word) {
        return toCompletableFuture(synonymizer.getSynonymsLemmas(SynonymRequest.newBuilder().setWord(word).build()))
                .thenApply(SynonymResponse::getSynonymsList)
                .thenApply(HashSet::new);
    }
}
