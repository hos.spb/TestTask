package com.randomm.searcher.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

/**
 * Search result data model ({@link com.randomm.searcher.controller.SearchController#search(String) /search})
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Search result data model")
public class SearchResponse {

    @Schema(description = "Found files list")
    @Singular
    private List<FileMatch> files;

}
