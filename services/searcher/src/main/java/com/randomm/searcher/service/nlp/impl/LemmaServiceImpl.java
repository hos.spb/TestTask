package com.randomm.searcher.service.nlp.impl;

import com.randomm.proto.Lemmatizer.LemmaRequest;
import com.randomm.proto.Lemmatizer.LemmaResponse;
import com.randomm.proto.lemmatizerGrpc;
import com.randomm.proto.lemmatizerGrpc.lemmatizerFutureStub;
import com.randomm.searcher.service.nlp.LemmaService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static net.javacrumbs.futureconverter.java8guava.FutureConverter.toCompletableFuture;

@Service
@RequiredArgsConstructor
public class LemmaServiceImpl implements LemmaService {

    @Value("${grpc.client.lemmatizer.host}")
    private String host;
    @Value("${grpc.client.lemmatizer.port}")
    private int port;

    private lemmatizerFutureStub lemmatizer;

    /**
     * Spring post init method
     * Initialize grpc client
     *
     * @see PostConstruct
     */
    @PostConstruct
    private void setup() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .defaultLoadBalancingPolicy("round_robin")
                .usePlaintext()
                .build();
        lemmatizer = lemmatizerGrpc.newFutureStub(channel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletableFuture<Set<String>> getLemmas(@NonNull String word) {
        return toCompletableFuture(lemmatizer.getLemmas(LemmaRequest.newBuilder().setWord(word).build()))
                .thenApply(LemmaResponse::getLemmasList)
                .thenApply(HashSet::new);
    }
}
