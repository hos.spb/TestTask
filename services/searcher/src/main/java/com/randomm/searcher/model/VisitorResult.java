package com.randomm.searcher.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Data model of visiting result
 */
@Getter
@RequiredArgsConstructor
public class VisitorResult {

    /**
     * The field contains a {@link Map map} with function as a key and set of intervals as a value.
     */
    private final Map<String, Set<Interval>> matches;

    public VisitorResult() {
        this.matches = new LinkedHashMap<>();
    }

    /**
     * Add match of query
     */
    public void addMatch(@NonNull String query, @NonNull Set<Interval> intervals) {
        matches.put(query, intervals);
    }
}
