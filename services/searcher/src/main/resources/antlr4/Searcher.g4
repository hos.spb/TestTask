grammar Searcher;

parse
 : expr EOF
 ;

expr
 : OPAR expr CPAR                           #exprPar
 | NOT function                             #not
 | left=expr AND right=expr                 #and
 | left=expr OR right=expr                  #or
 | function #func
 ;

function
 : OPAR function CPAR                       #funcPar
 | SYNONIM OPAR WORD CPAR                   #syn
 | LEMMA OPAR WORD CPAR                     #lemm
 | LEVENSHTEIN OPAR WORD COMMA INT CPAR     #levenshtein
 ;


SYNONIM : 'syn';
LEVENSHTEIN : 'levenshtein';
LEMMA : 'lemm';

OR : 'or';
AND : 'and';
NOT : 'not';

OPAR : '(';
CPAR : ')';
COMMA : ',';

INT: [0-9]+;

WORD : [a-zA-Z]+;

//skip tabulation, spaces and new lines
SPACE : [ \t\r\n] -> skip;