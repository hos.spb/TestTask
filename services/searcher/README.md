# Searcher service

## Description

The service searches among the source texts according to the specified query.

For correct work of the service, other services are also required, see. [lemmatizer](../lemmatizer/README.md)
and [synonymizer](../synonymizer/README.md)
Communication with the service is carried out via [REST API](http://localhost:50100/swagger-ui.html).

### Search expressions:

Expression is various combinations of functions, logical operations and parentheses.

The service supports:

* functions:
    * `lemm(word)` - search for words whose lemmas coincide with the lemma of the given word
    * `syn(word)` - search for words whose lemmas coincide with the lemmas of synonyms of the given word
    * `levenshtein(word,2)` - fuzzy search for words, the Levenshtein distance from which to the given one(word) is less
      than or equal to a given number(2)

* logical operations:
    * `x and y` - will find those files on which both expression x and expression y are correct.
    * `x or y` - will find those files for which either the x expression, or the y expression, or both are true.
    * `not x` - will find those files for which the expression x is not executed (only a function can be as x)
* parentheses - `(`x`)`- for visually differentiate and change the priority of operations

For all operations as word may be a word contains only english characters without spaces or another symbols

#### A few examples

* `not(lemm(bad))` - find those files that do not contain words with lemmas of word 'bad'
* `(lemm(he) or levenshtein(that,3)) and syn(san)` - find those files that contain words with lemma he or
* the Levenshtein distance from which to the word that is at most 3, and the words of whose lemmas coincide with the
  lemmas of the synonyms san

## Libraries used

The service is based on spring, grpc and antlr4. See also [pom.xml](pom.xml)

## Running the service

### Local

Ensure that you have installed jdk 11+ and maven 3.6.3+

To run the service go to the appropriate directory

   ```
   cd services/searcher
   ```

and run:

   ```
   mvn clwan package
   mvn spring-boot:run
   ```

### Docker

To run the service:

```
docker-compose build searcher
docker-compose up searcher
```

## Service Usage

After running the service it's available by default on [localhost:50100](http://localhost:50100/)
For start searching type an expression to web form and press enter or using something like Postman send request
directly.

### Source text files

#### Local

The files are taken from the directory at the root of the project at /sourceTexts. That can be configured
in [application.yml](src/main/resources/application.yml) configuration file.

#### Docker

When run via docker-compose by default the docker volume of the directory is created at the root of the project at
/sourceTexts